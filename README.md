# File Uploader
File upload with local and s3 (Minio)

# Cấu hình
1. PHP >= 7.1
2. workable/core: `composer require workable/core`
3. workable/helper: `composer require workable/helper`
4. intervention/image:^2.5: `composer require intervention/image:^2.5`
5. league/flysystem-aws-s3-v3: `composer require league/flysystem-aws-s3-v3`
6. Extensions php ready: `imagick`
7. Cài theo `install.md`
8. Xem hướng dẫn sử dụng `document.md`


Functions | Description | Param
--- | --- | ---
upload | Cho phép upload một hình ảnh | $fileControl, $arrayThumbs = [], $optional = 'resize'
uploadMulti | Cho phép upload nhiều hình ảnh  |  $fileControl, $arrayThumbs = [], $optional = 'resize'
uploadFromLink | Cho phép upload một hình ảnh từ đường dẫn link hình ảnh | $link, $arrayThumbs = [], $optional = 'resize'
uploadBase64 | Cho phép upload hình ảnh là base64. Thường áp dụng cho upload jax | $fileBase64, $arrayThumbs = [], $optional = 'resize'


Ghi chú: Các function trên khi upload sẽ chèn các tham số vào. Các tham số này là:
- `$fileControl` : Tên thuộc tính `name` của input type=file
- `$arrayThumbs` : mảng thumb truyền vào. Xem thêm config upload image
- `$optional` : Cho phép resize hình ảnh hay không nếu fit không phù hợp

# Tính năng

Functions | Description | Param
--- | --- | ---
upload | Cho phép upload một file | $fileControl
uploadMulti | Cho phép upload nhiều files  |  $fileControl
uploadFromLink | Cho phép upload một files từ đường dẫn link | $link, $param = []


Support | Driver Local | Driver Minio
--- | --- | ---
Upload       |  [x]  | [x]
Base 64 encode  | [x]  | [x]
Upload from link  |  [x]  | [x]
Upload Multiple  |  [x]  | [x]
Format: 'jpg', 'jpeg', 'png', 'gif', 'webp'  | [x]  | [x]
Not enable: css,js, php. |  [x]  | [x]
Limit size  | [x] |  [x]
Resize thumb  | [x] |  [x]
Crop thumb   | [x] |  [x]
Upload link webp: upload/crop/resize | [x] | [x]
convert link jpg/png/jpeg/gif to webp | [x] | [x]
Watermark     | [] |  []

# Todo task
- [] Upload nhiều server
- [] Parse Url ra thành nhiều server.


# Testing
Cấu hình file sau vào file `phpunit.xml` của dự án để có thể chạy testcase.
```
<testsuite name="UploadFile">
    <directory suffix=".php">./platform/packages/file-uploader/tests/Feature</directory>
</testsuite>
```

Run testcase cho toàn bộ upload files.

```
./vendor/bin/phpunit --testsuite=UploadFile --testdox
```