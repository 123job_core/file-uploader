<?php

namespace Workable\FileUploader\Core\Driver;

use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Storage;
use Workable\FileUploader\Core\Contracts\UploadDriverInterface;
use Workable\FileUploader\Core\Enum\UploadEnum;
use Workable\FileUploader\Core\Exceptions\UploadFileException;
use Workable\FileUploader\Core\Utils\ResponseUpload;
use Workable\FileUploader\Core\Utils\DataUpload;

class UploadDefault implements UploadDriverInterface
{
    public function upload(DataUpload $dataUpload, $params = [])
    {
        $newFile    = $dataUpload->getNewFile();
        $pathUpload = $dataUpload->getUploadFolderToday();
        $fullPath   = $dataUpload->getFullPathNewFile($pathUpload);
        $diskName   = $dataUpload->getDiskName();

        try
        {
            if ($dataUpload->getContentTmpNameFile())
            {
                Storage::disk($diskName)->put($fullPath, $dataUpload->getContentTmpNameFile());

                $fileSize   = filesize($this->getFullPath($fullPath));
                $pathUpload = $this->getFullPath($pathUpload);
                $responseUpload = new ResponseUpload($newFile, $pathUpload , $fileSize , UploadEnum::DRIVER_LOCAL);
                $responseUpload
                    ->setFileNameUpload($dataUpload->getFileName())
                    ->setExtensionUpload($dataUpload->getExtension());

                return $responseUpload;
            }
        }
        catch (UploadFileException $e)
        {
            Log::error('-- Error upload default: ' . $e->info());
        }

        return new ResponseUpload();
    }

    public function getFullPath($fullPath)
    {
        $configFileSystem = config("filesystems");
        $defaultDriver    = $configFileSystem['default'];
        $root = $configFileSystem["disks"][$defaultDriver]['root'];

        return rtrim($root) . "/". ltrim($fullPath, "/");
    }

    public function delete($dataDelete=[])
    {
        try
        {
            $diskName = $dataDelete['drive']['disk_name'];
            $path     = $dataDelete['path'];
            Storage::disk($diskName)->delete($path);

            return 1;
        }
        catch (UploadFileException $e)
        {
            Log::error('-- Error delete: ' . $e->info());
        }
        return 1;
    }
}
