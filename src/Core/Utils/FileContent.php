<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 6/26/19
 * Time: 13:53
 */

namespace Workable\FileUploader\Core\Utils;


use Illuminate\Support\Arr;

class FileContent
{
    private static $contentLink = null;

    private static $instance = null;

    private static $contentType = null;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getContent($link, $contentTypeCheck = false)
    {
        $arrContextOptions = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
            ],
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        $content = file_get_contents($link, false, stream_context_create($arrContextOptions));

        // Content type
        if ($contentTypeCheck) {
            try {
                $ext = pathinfo(parse_url($link, PHP_URL_PATH), PATHINFO_EXTENSION);
                if ($ext) {
                    self::$contentType = "ext/" . $ext;
                }
                else {
                    $pattern = "/^content-type\s*:\s*(.*)$/i";
                    $header  = array_values(preg_grep($pattern, $http_response_header));
                    $header  = Arr::last($header);
                    if ($header && (preg_match($pattern, $header, $match) !== false)) {
                        self::$contentType = $match[1];
                    }
                }
            }
            catch (\Exception $e) {
                \CliEcho::warningnl($e->getMessage());
            }
        }

        self::$contentLink = $content;

        return self::$contentLink;
    }

    public function get()
    {
        return self::$contentLink;
    }

    public function getMimeType($link)
    {
        $this->getContent($link, true);
        $mime = self::$contentType;
        $mime = $mime ? explode("/", $mime) : [];
        $mime = $mime[1] ?? null;

        return $mime;
    }
}
