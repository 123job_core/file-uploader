<?php
/**
 * Class response upload from driver
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 8/15/20 - 07:25
 */
namespace Workable\FileUploader\Core\Utils;


class ResponseUpload
{
    /**
     * Kết quả tện file mới tạo trả về từ upload
     * @var string
     */
    protected $files       = '';

    /**
     * Đường dẫn upload
     * @var string
     */
    protected $path_upload = '';

    /**
     * Kích thước upload
     * @var string
     */
    protected $size        = '';

    /**
     * @var string Drive upload
     */
    protected $driver      = '';

    /**
     * Tên file upload
     * @var string
     */
    protected $fileName = '';


    /**
     * Tên file trước khi tải lên
     * @var string
     */
    protected $fileNameDefault;

    /**
     * Extension upload
     * @var string
     */
    protected $extension = '';

    /**
     * @var array
     */
    protected $driverConfig = [];


    public function __construct($files = '', $path_upload = '', $size = '', $driver='', $driverConfig = [])
    {
        $this->files       = $files;
        $this->path_upload = $path_upload;
        $this->size        = $size;
        $this->driver      = $driver;
        $this->driverConfig = $driverConfig;
    }


    public function setFileNameUpload($file = '')
    {
        $this->fileName = $file;
        return $this;
    }

    public function setExtensionUpload($ext = '')
    {
        $this->extension = $ext;
        return $this;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getPathUpload()
    {
        return $this->path_upload;
    }

    public function getSizeUpload()
    {
        return $this->size;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function getDriverConfig()
    {
        return $this->driverConfig;
    }

    public function getName()
    {
        return $this->fileName;
    }

    public function getExtension()
    {
        return $this->extension;
    }

    public function toArray()
    {
        return [
            'file_name'           => $this->files,
            'size'                => $this->size,
            'driver'              => $this->driver,
            "name"                => $this->fileName,
            "extension"           => $this->extension
        ];
    }
}
