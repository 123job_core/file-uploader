<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 2020/08/15 - 14:50
 */
namespace Workable\FileUploader\Services;

use Workable\Base\Supports\CliEcho;
use Workable\FileUploader\Core\Exceptions\UploadFileException;
use Workable\FileUploader\Supports\FileUploaderConfigSupport;
use ImageUploader;

class ImageUploaderService
{
    protected static $abstract = 'image-uploader';

    protected static $messageError;

    /**
     * @param $config
     * @return \Workable\FileUploader\Core\Uploader\ImageUploader
     */
    protected static function instanceFactory($config)
    {
        return ImageUploader::setConfig($config);
    }

    public static function upload($fileControl, $type = 'logo', $optional = '')
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_image.'.$type);
            $thumbs  = $config['thumbs'] ?? null;
            $results = static::instanceFactory($config)->upload($fileControl, $thumbs, $optional);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function uploadMulti($fileControl, $type='logo')
    {
        try
        {
            $config = FileUploaderConfigSupport::get('upload_image.'.$type);
            $thumbs  = $config['thumbs'] ?? null;
            $results = static::instanceFactory($config)->uploadMulti($fileControl, $thumbs);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function uploadFromLink($link, $type, $optional = '')
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_image.'.$type);
            $thumbs  = $config['thumbs'] ?? null;
            $results = static::instanceFactory($config)->uploadFromLink($link, $thumbs, $optional);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function uploadFromBase64($fileBase64, $type, $optional = '')
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_image.'.$type);
            $thumbs  = $config['thumbs'] ?? null;

            $results = static::instanceFactory($config)->uploadBase64($fileBase64, $thumbs, $optional);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function deleteImage($fileName, $type = 'logo') {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_image.'.$type);
            $results = static::instanceFactory($config)->deleteImage($fileName);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    private static function setMessageError($message)
    {
        self::$messageError = $message;
    }

    protected static function logging(UploadFileException $e)
    {
        CliEcho::errornl($e->info());
    }
}
