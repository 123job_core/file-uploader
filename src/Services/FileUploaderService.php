<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 2020/08/15 - 14:50
 */
namespace Workable\FileUploader\Services;

use Workable\Base\Supports\CliEcho;
use Workable\FileUploader\Core\Exceptions\UploadFileException;
use Workable\FileUploader\Supports\FileUploaderConfigSupport;
use Uploader;

class FileUploaderService
{
    protected static $messageError;
    protected static $abstract = 'uploader';

    /**
     * @return \Workable\FileUploader\Core\Uploader\Uploader
     */
    protected static function instanceFactory($config)
    {
        return Uploader::setConfig($config);
    }

    public static function upload($fileControl, $key='pdf_and_doc')
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_file.'.$key);
            $results = self::instanceFactory($config)->upload($fileControl);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function uploadMulti($fileControl, $key = 'pdf_and_doc')
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_file.'.$key);
            $results = self::instanceFactory($config)->uploadMulti($fileControl);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function uploadFileBase64($base64, $key, $optional = [])
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_file.'.$key);
            $results = static::instanceFactory($config)->uploadFileBase64($base64, $optional);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    public static function uploadFromLink($link, $key = 'pdf_and_doc')
    {
        try
        {
            $config  = FileUploaderConfigSupport::get('upload_file.'.$key);
            $results = static::instanceFactory($config)->uploadFromLink($link);
        }
        catch (UploadFileException $e)
        {
            self::setMessageError($e->getMessage());
            $results = null;
        }

        return $results;
    }

    private static function setMessageError($message)
    {
        self::$messageError = $message;
    }

    protected static function logging(UploadFileException $e)
    {
        CliEcho::errornl($e->info());
    }
}
