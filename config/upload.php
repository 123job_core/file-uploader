<?php

return [
    // Các file được phép upload
    'extensions'    => ['gif', 'jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf', 'bmp', 'js', 'css', 'webp'],
    'file_size'     => 2048,
    'upload_folder' => 'uploads',
    'static_url'    => env('AWS_ENDPOINT'),
    'default'       => env('DRIVER_UPLOAD', 'local'),
    'driver'        => [
        'local' => [
            'disk_name'        => \Workable\FileUploader\Core\Enum\UploadEnum::DISK_PUBLIC,
            'root'             => base_path(),
            'path'             => '',
            'url'              => '',
        ],
        'minio'   => [
            'disk_name'               => \Workable\FileUploader\Core\Enum\UploadEnum::DISK_MINIO,
            'use_path_style_endpoint' => true,
            'bucket'                  => env('AWS_BUCKET'),
            'url'                     => env('AWS_ENDPOINT'),
            'endpoint'                => env('AWS_ENDPOINT'),
            'path'                    => '',
            'root'                    => '/'
        ]
    ]
];
