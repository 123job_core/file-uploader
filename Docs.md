# Upload hình ảnh: Jpg, Png, Jpeg
```
    # via service container
    app("image-uploader")->setConfig($config)->upload($fileControl)

    # via facade
    use ImageUploader;
    ImageUploader::setConfig($config)->upload($fileControl);   
```


# Upload bằng file: pdf, docs, .xls
```
    # via service container
    app("uploader")->setConfig($config)->upload($fileControl)

    # via facade
    use Uploader;
    Uploader::setConfig($config)->upload($fileControl);
```

> Kết qủa upload sẽ là null nếu lỗi hoặc có kết quả thì sẽ là instance của Workable\FileUploader\Core\Utils\ResultUpload


# Cách parse đường dẫn để lấy thông tin url
```
    $url = 'md_2021_02_11______07c2c9771351edf9faabfec65a32a415.jpg';
    $link = parse_url_file($url,'uploads');
```
> Tham số thứ hai của parse_url_file chính là thư mục đã tải lên.