# Cài đặt 1
```
# Add submodule
    git submodule add git@gitlab.com:workable_packages/file-uploader.git platform/packages/file-uploader

# Init repository in composer.json
  "repositories": [
        {
            "type": "path",
            "url": "./platform/packages/*",
            "options": {
                "symlink": true
            }
        }
    ]

# Install composer
    composer require workable/file-uploader:@dev

# Publish package
    php artisan vendor:publish --provider="Workable\FileUploader\FileUploaderServiceProvider" --tag="config"

```

# Config cấu hình driver upload
Mục tiêu đoạn này để mở rộng driver. Thay vì sử dụng mặc định thì mình sẽ sử dụng thêm driver minio để upload qua s3.


Config file  `config/filesystems.php`
```
[
	'public' => [
	    'driver' => 'local',
	    'root' => public_path(),
	    'url' => env('APP_URL').'/storage',
	    'visibility' => 'public',
	],

	'minio' => [
	    'driver'                  => 's3',
	    'endpoint'                => env('AWS_ENDPOINT', 'https://cdn.domain.vn/'),
	    'use_path_style_endpoint' => true,
	    'key'                     => env('AWS_KEY'),
	    'secret'                  => env('AWS_SECRET'),
	    'region'                  => env('AWS_REGION'),
	    'bucket'                  => env('AWS_BUCKET','123job'),
	    'url'                     => env('AWS_ENDPOINT', 'https://cdn.domain.vn/')
	]
]
```

# Config cấu hình upload

*Các thông số*
- `extensions`: Các định dạng cho phép upload
- `file_size`: kích thước file được upload tính bằng KB
- `upload_folder`: folder chứa file uploaders
- `check_resize`: Kiểm tra cho phép resize nếu định dạng width vs height k khớp. Mặc định là 0
- `thumbs`: Các thumbs sẽ được resize
- `logo` vs `pdf_and_doc` : Các key sẽ được sử dụng cho upload cụ thể chức năng gì.



# Config cấu hình env
*Sử dụng upload default: Upload ảnh ở local*
DRIVER_UPLOAD=local
```
# UPLOAD LOCAL

```



*Sử dụng upload cdn: Upload ảnh ở server*
```
# UPLOAD
DRIVER_UPLOAD=minio
AWS_ENDPOINT=https://cdn.domain.vn/
AWS_REGION=vi-east-1
AWS_BUCKET=123job
AWS_KEY=minion123job
AWS_SECRET=matkhau
```

